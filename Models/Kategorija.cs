﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class Kategorija
    {
        public Kategorija()
        {
            Podrucje = new HashSet<Podrucje>();
        }

        public int IdKategorija { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }

        public virtual ICollection<Podrucje> Podrucje { get; set; }
    }
}
