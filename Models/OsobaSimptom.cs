﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class OsobaSimptom
    {
        public int IdOsoba { get; set; }
        public int IdSimptom { get; set; }

        public virtual Osoba IdOsobaNavigation { get; set; }
        public virtual Simptom IdSimptomNavigation { get; set; }
    }
}
