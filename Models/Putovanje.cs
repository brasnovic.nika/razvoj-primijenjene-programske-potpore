﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class Putovanje
    {
        public Putovanje()
        {
            PutovanjePodrucje = new HashSet<PutovanjePodrucje>();
        }

        public int IdPutovanje { get; set; }
        public DateTime DatPocetak { get; set; }
        public DateTime? DatKraj { get; set; }
        public int IdOsoba { get; set; }

        public virtual Osoba IdOsobaNavigation { get; set; }
        public virtual ICollection<PutovanjePodrucje> PutovanjePodrucje { get; set; }
    }
}
