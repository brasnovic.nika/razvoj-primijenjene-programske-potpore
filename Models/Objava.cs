﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class Objava
    {
        public int IdObjave { get; set; }
        public int IdOrganizacija { get; set; }
        public int IdTerminaObjave { get; set; }
        public int? BrojZarazenih { get; set; }
        public int? BrojKriticnih { get; set; }
        public int? BrojIzljecenih { get; set; }
        public int? BrojUmrlih { get; set; }

        public virtual Organizacija IdOrganizacijaNavigation { get; set; }
        public virtual TerminObjave IdTerminaObjaveNavigation { get; set; }
    }
}
