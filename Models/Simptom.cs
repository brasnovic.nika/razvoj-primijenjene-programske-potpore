﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class Simptom
    {
        public Simptom()
        {
            OsobaSimptom = new HashSet<OsobaSimptom>();
        }

        public int IdSimptom { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public string Kategorija { get; set; }
        public int? IdOzbiljnostSimptoma { get; set; }
        public bool? PotrebanRespirator { get; set; }
        public bool? PotrebnaUmjetnaPluca { get; set; }
        public bool? MoguceKomplikacije { get; set; }
        public string LokacijaLijecenja { get; set; }

        public virtual OzbiljnostSimptoma IdOzbiljnostSimptomaNavigation { get; set; }
        public virtual ICollection<OsobaSimptom> OsobaSimptom { get; set; }
    }
}
