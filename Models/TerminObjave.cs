﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class TerminObjave
    {
        public TerminObjave()
        {
            Objava = new HashSet<Objava>();
        }

        public int IdTerminaObjave { get; set; }
        public string Termin { get; set; }

        public virtual ICollection<Objava> Objava { get; set; }
    }
}
