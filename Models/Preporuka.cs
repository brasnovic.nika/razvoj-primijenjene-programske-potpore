﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class Preporuka
    {
        public int IdPreporuka { get; set; }
        public string Opis { get; set; }
        public DateTime? DatumIzdavanja { get; set; }
        public DateTime? RokVazenja { get; set; }
        public int IdStozer { get; set; }
        public int IdStatusPreporuke { get; set; }

        public virtual StatusPreporuke IdStatusPreporukeNavigation { get; set; }
        public virtual Stozer IdStozerNavigation { get; set; }
    }
}
