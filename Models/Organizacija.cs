﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class Organizacija
    {
        public Organizacija()
        {
            Objava = new HashSet<Objava>();
            Osoba = new HashSet<Osoba>();
            Podruznica = new HashSet<Podruznica>();
        }

        public int IdOrganizacija { get; set; }
        public string Naziv { get; set; }
        public string AdresaSjedista { get; set; }
        public string WebUrl { get; set; }
        public string Opis { get; set; }
        public int? IdPredsjednika { get; set; }
        public int IdPodrucje { get; set; }

        public virtual Podrucje IdPodrucjeNavigation { get; set; }
        public virtual Osoba IdPredsjednikaNavigation { get; set; }
        public virtual ICollection<Objava> Objava { get; set; }
        public virtual ICollection<Osoba> Osoba { get; set; }
        public virtual ICollection<Podruznica> Podruznica { get; set; }
    }
}
