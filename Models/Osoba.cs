﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class Osoba
    {
        public Osoba()
        {
            Organizacija = new HashSet<Organizacija>();
            OsobaSimptom = new HashSet<OsobaSimptom>();
            Putovanje = new HashSet<Putovanje>();
            Stozer = new HashSet<Stozer>();
        }

        public int IdOsoba { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Zanimanje { get; set; }
        public string BrojPutovnice { get; set; }
        public DateTime DatumRodenja { get; set; }
        public int Dob { get; set; }
        public string MjestoRodenja { get; set; }
        public string Adresa { get; set; }
        public string MjestoStanovanja { get; set; }
        public string BrojTelefona { get; set; }
        public int? IdOrganizacija { get; set; }
        public string OpceStanje { get; set; }
        public string KlinickaSlika { get; set; }

        public virtual Organizacija IdOrganizacijaNavigation { get; set; }
        public virtual ICollection<Organizacija> Organizacija { get; set; }
        public virtual ICollection<OsobaSimptom> OsobaSimptom { get; set; }
        public virtual ICollection<Putovanje> Putovanje { get; set; }
        public virtual ICollection<Stozer> Stozer { get; set; }
    }
}
