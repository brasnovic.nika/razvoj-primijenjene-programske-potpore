﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class OzbiljnostSimptoma
    {
        public OzbiljnostSimptoma()
        {
            Simptom = new HashSet<Simptom>();
        }

        public int IdOzbiljnostSimptoma { get; set; }
        public string OzbiljnostSimptoma1 { get; set; }

        public virtual ICollection<Simptom> Simptom { get; set; }
    }
}
