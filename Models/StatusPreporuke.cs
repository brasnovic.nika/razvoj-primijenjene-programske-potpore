﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class StatusPreporuke
    {
        public StatusPreporuke()
        {
            Preporuka = new HashSet<Preporuka>();
        }

        public int IdStatusPreporuke { get; set; }
        public string Naziv { get; set; }

        public virtual ICollection<Preporuka> Preporuka { get; set; }
    }
}
