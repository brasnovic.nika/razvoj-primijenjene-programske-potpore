﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace RPPP01.Models
{
    public partial class RPPP01Context : DbContext
    {
        

        public RPPP01Context(DbContextOptions<RPPP01Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Kategorija> Kategorija { get; set; }
        public virtual DbSet<Objava> Objava { get; set; }
        public virtual DbSet<Organizacija> Organizacija { get; set; }
        public virtual DbSet<Osoba> Osoba { get; set; }
        public virtual DbSet<OsobaSimptom> OsobaSimptom { get; set; }
        public virtual DbSet<OzbiljnostSimptoma> OzbiljnostSimptoma { get; set; }
        public virtual DbSet<Podrucje> Podrucje { get; set; }
        public virtual DbSet<Podruznica> Podruznica { get; set; }
        public virtual DbSet<Preporuka> Preporuka { get; set; }
        public virtual DbSet<Putovanje> Putovanje { get; set; }
        public virtual DbSet<PutovanjePodrucje> PutovanjePodrucje { get; set; }
        public virtual DbSet<Simptom> Simptom { get; set; }
        public virtual DbSet<StatusPreporuke> StatusPreporuke { get; set; }
        public virtual DbSet<Stozer> Stozer { get; set; }
        public virtual DbSet<TerminObjave> TerminObjave { get; set; }

       

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Kategorija>(entity =>
            {
                entity.HasKey(e => e.IdKategorija)
                    .HasName("PK__tmp_ms_x__F29872CCFD46E8CD");

                entity.Property(e => e.IdKategorija).HasColumnName("idKategorija");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasColumnName("naziv")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Opis)
                    .IsRequired()
                    .HasColumnName("opis")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Objava>(entity =>
            {
                entity.HasKey(e => e.IdObjave)
                    .HasName("PK__Objava__C139E28E2A00290D");

                entity.HasIndex(e => e.IdOrganizacija);

                entity.HasIndex(e => e.IdTerminaObjave);

                entity.Property(e => e.IdObjave).HasColumnName("idObjave");

                entity.Property(e => e.BrojIzljecenih).HasColumnName("brojIzljecenih");

                entity.Property(e => e.BrojKriticnih).HasColumnName("brojKriticnih");

                entity.Property(e => e.BrojUmrlih).HasColumnName("brojUmrlih");

                entity.Property(e => e.BrojZarazenih).HasColumnName("brojZarazenih");

                entity.Property(e => e.IdOrganizacija).HasColumnName("idOrganizacija");

                entity.Property(e => e.IdTerminaObjave).HasColumnName("idTerminaObjave");

                entity.HasOne(d => d.IdOrganizacijaNavigation)
                    .WithMany(p => p.Objava)
                    .HasForeignKey(d => d.IdOrganizacija)
                    .HasConstraintName("FK__Objava__idOrgani__72D0F942");

                entity.HasOne(d => d.IdTerminaObjaveNavigation)
                    .WithMany(p => p.Objava)
                    .HasForeignKey(d => d.IdTerminaObjave)
                    .HasConstraintName("FK__Objava__idTermin__73C51D7B");
            });

            modelBuilder.Entity<Organizacija>(entity =>
            {
                entity.HasKey(e => e.IdOrganizacija)
                    .HasName("PK__Organiza__E3128A79991623E7");

                entity.HasIndex(e => e.IdPodrucje);

                entity.HasIndex(e => e.IdPredsjednika);

                entity.Property(e => e.IdOrganizacija).HasColumnName("idOrganizacija");

                entity.Property(e => e.AdresaSjedista)
                    .HasColumnName("adresaSjedista")
                    .HasMaxLength(128);

                entity.Property(e => e.IdPodrucje).HasColumnName("idPodrucje");

                entity.Property(e => e.IdPredsjednika).HasColumnName("idPredsjednika");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasColumnName("naziv")
                    .HasMaxLength(128);

                entity.Property(e => e.Opis)
                    .HasColumnName("opis")
                    .HasColumnType("text");

                entity.Property(e => e.WebUrl)
                    .HasColumnName("webUrl")
                    .HasMaxLength(128);

                entity.HasOne(d => d.IdPodrucjeNavigation)
                    .WithMany(p => p.Organizacija)
                    .HasForeignKey(d => d.IdPodrucje)
                    .HasConstraintName("FK_Organizacija_idPodrucje");

                entity.HasOne(d => d.IdPredsjednikaNavigation)
                    .WithMany(p => p.Organizacija)
                    .HasForeignKey(d => d.IdPredsjednika)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("Organizacija_Osoba__fk");
            });

            modelBuilder.Entity<Osoba>(entity =>
            {
                entity.HasKey(e => e.IdOsoba)
                    .HasName("Osoba_PK");

                entity.HasIndex(e => e.IdOrganizacija);

                entity.Property(e => e.IdOsoba).HasColumnName("idOsoba");

                entity.Property(e => e.Adresa)
                    .IsRequired()
                    .HasColumnName("adresa")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BrojPutovnice)
                    .IsRequired()
                    .HasColumnName("brojPutovnice")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BrojTelefona)
                    .HasColumnName("brojTelefona")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DatumRodenja)
                    .HasColumnName("datumRodenja")
                    .HasColumnType("date");

                entity.Property(e => e.Dob).HasColumnName("dob");

                entity.Property(e => e.IdOrganizacija).HasColumnName("idOrganizacija");

                entity.Property(e => e.Ime)
                    .IsRequired()
                    .HasColumnName("ime")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.KlinickaSlika)
                    .HasColumnName("klinickaSlika")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MjestoRodenja)
                    .IsRequired()
                    .HasColumnName("mjestoRodenja")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MjestoStanovanja)
                    .IsRequired()
                    .HasColumnName("mjestoStanovanja")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OpceStanje)
                    .HasColumnName("opceStanje")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Prezime)
                    .IsRequired()
                    .HasColumnName("prezime")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Zanimanje)
                    .IsRequired()
                    .HasColumnName("zanimanje")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdOrganizacijaNavigation)
                    .WithMany(p => p.Osoba)
                    .HasForeignKey(d => d.IdOrganizacija)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK_Osoba_Organizacija");
            });

            modelBuilder.Entity<OsobaSimptom>(entity =>
            {
                entity.HasKey(e => new { e.IdOsoba, e.IdSimptom })
                    .HasName("OsobaSimptom_PK");

                entity.HasIndex(e => e.IdSimptom);

                entity.Property(e => e.IdOsoba).HasColumnName("idOsoba");

                entity.Property(e => e.IdSimptom).HasColumnName("idSimptom");

                entity.HasOne(d => d.IdOsobaNavigation)
                    .WithMany(p => p.OsobaSimptom)
                    .HasForeignKey(d => d.IdOsoba)
                    .HasConstraintName("OsobaSimptom_FK_1");

                entity.HasOne(d => d.IdSimptomNavigation)
                    .WithMany(p => p.OsobaSimptom)
                    .HasForeignKey(d => d.IdSimptom)
                    .HasConstraintName("OsobaSimptom_FK");
            });

            modelBuilder.Entity<OzbiljnostSimptoma>(entity =>
            {
                entity.HasKey(e => e.IdOzbiljnostSimptoma)
                    .HasName("OzbiljnostSimptoma_PK");

                entity.Property(e => e.IdOzbiljnostSimptoma).HasColumnName("idOzbiljnostSimptoma");

                entity.Property(e => e.OzbiljnostSimptoma1)
                    .IsRequired()
                    .HasColumnName("ozbiljnostSimptoma")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Podrucje>(entity =>
            {
                entity.HasKey(e => e.IdPodrucje)
                    .HasName("PK__tmp_ms_x__24F6B2EA732502B4");

                entity.HasIndex(e => e.IdKategorija);

                entity.Property(e => e.IdPodrucje).HasColumnName("idPodrucje");

                entity.Property(e => e.Drzava)
                    .IsRequired()
                    .HasColumnName("drzava")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdKategorija).HasColumnName("idKategorija");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasColumnName("naziv")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Stanovnistvo).HasColumnName("stanovnistvo");

                entity.HasOne(d => d.IdKategorijaNavigation)
                    .WithMany(p => p.Podrucje)
                    .HasForeignKey(d => d.IdKategorija)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK_Podrucje_Kategorija");
            });

            modelBuilder.Entity<Podruznica>(entity =>
            {
                entity.HasKey(e => e.IdPodruznica)
                    .HasName("PK__Podruzni__E499C1D3D903109E");

                entity.HasIndex(e => e.IdOrganizacija);

                entity.Property(e => e.IdPodruznica).HasColumnName("idPodruznica");

                entity.Property(e => e.Adresa)
                    .HasColumnName("adresa")
                    .HasMaxLength(128);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(128);

                entity.Property(e => e.IdOrganizacija).HasColumnName("idOrganizacija");

                entity.Property(e => e.Telefon)
                    .HasColumnName("telefon")
                    .HasMaxLength(128);

                entity.HasOne(d => d.IdOrganizacijaNavigation)
                    .WithMany(p => p.Podruznica)
                    .HasForeignKey(d => d.IdOrganizacija)
                    .HasConstraintName("FK__Podruznic__idOrg__5575A085");
            });

            modelBuilder.Entity<Preporuka>(entity =>
            {
                entity.HasKey(e => e.IdPreporuka)
                    .HasName("PK__Preporuk__F47895CF812FE2AB");

                entity.HasIndex(e => e.IdStatusPreporuke);

                entity.HasIndex(e => e.IdStozer);

                entity.Property(e => e.IdPreporuka).HasColumnName("idPreporuka");

                entity.Property(e => e.DatumIzdavanja)
                    .HasColumnName("datumIzdavanja")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdStatusPreporuke).HasColumnName("idStatusPreporuke");

                entity.Property(e => e.IdStozer).HasColumnName("idStozer");

                entity.Property(e => e.Opis)
                    .HasColumnName("opis")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RokVazenja)
                    .HasColumnName("rokVazenja")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.IdStatusPreporukeNavigation)
                    .WithMany(p => p.Preporuka)
                    .HasForeignKey(d => d.IdStatusPreporuke)
                    .HasConstraintName("FK__Preporuka__idSta__59D0414E");

                entity.HasOne(d => d.IdStozerNavigation)
                    .WithMany(p => p.Preporuka)
                    .HasForeignKey(d => d.IdStozer)
                    .HasConstraintName("FK__Preporuka__idSto__58DC1D15");
            });

            modelBuilder.Entity<Putovanje>(entity =>
            {
                entity.HasKey(e => e.IdPutovanje)
                    .HasName("PK__tmp_ms_x__E442932E5DF46915");

                entity.HasIndex(e => e.IdOsoba);

                entity.Property(e => e.IdPutovanje).HasColumnName("idPutovanje");

                entity.Property(e => e.DatKraj)
                    .HasColumnName("datKraj")
                    .HasColumnType("date");

                entity.Property(e => e.DatPocetak)
                    .HasColumnName("datPocetak")
                    .HasColumnType("date");

                entity.Property(e => e.IdOsoba).HasColumnName("idOsoba");

                entity.HasOne(d => d.IdOsobaNavigation)
                    .WithMany(p => p.Putovanje)
                    .HasForeignKey(d => d.IdOsoba)
                    .HasConstraintName("FK_Putovanje_Osoba");
            });

            modelBuilder.Entity<PutovanjePodrucje>(entity =>
            {
                entity.HasKey(e => new { e.IdPutovanje, e.IdPodrucje })
                    .HasName("PK__Putovanj__560DF800F97DA30E");

                entity.HasIndex(e => e.IdPodrucje);

                entity.Property(e => e.IdPutovanje).HasColumnName("idPutovanje");

                entity.Property(e => e.IdPodrucje).HasColumnName("idPodrucje");

                entity.HasOne(d => d.IdPodrucjeNavigation)
                    .WithMany(p => p.PutovanjePodrucje)
                    .HasForeignKey(d => d.IdPodrucje)
                    .HasConstraintName("FK_PutovanjePodrucje_Podrucje");

                entity.HasOne(d => d.IdPutovanjeNavigation)
                    .WithMany(p => p.PutovanjePodrucje)
                    .HasForeignKey(d => d.IdPutovanje)
                    .HasConstraintName("FK_PutovanjePodrucje_Putovanje");
            });

            modelBuilder.Entity<Simptom>(entity =>
            {
                entity.HasKey(e => e.IdSimptom)
                    .HasName("Simptom_PK");

                entity.HasIndex(e => e.IdOzbiljnostSimptoma);

                entity.Property(e => e.IdSimptom).HasColumnName("idSimptom");

                entity.Property(e => e.IdOzbiljnostSimptoma).HasColumnName("idOzbiljnostSimptoma");

                entity.Property(e => e.Kategorija)
                    .IsRequired()
                    .HasColumnName("kategorija")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LokacijaLijecenja)
                    .IsRequired()
                    .HasColumnName("lokacijaLijecenja")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MoguceKomplikacije).HasColumnName("moguceKomplikacije");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasColumnName("naziv")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Opis)
                    .HasColumnName("opis")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PotrebanRespirator).HasColumnName("potrebanRespirator");

                entity.Property(e => e.PotrebnaUmjetnaPluca).HasColumnName("potrebnaUmjetnaPluca");

                entity.HasOne(d => d.IdOzbiljnostSimptomaNavigation)
                    .WithMany(p => p.Simptom)
                    .HasForeignKey(d => d.IdOzbiljnostSimptoma)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("Simptom_FK");
            });

            modelBuilder.Entity<StatusPreporuke>(entity =>
            {
                entity.HasKey(e => e.IdStatusPreporuke)
                    .HasName("PK__StatusPr__0DDEF4DECE1FF879");

                entity.Property(e => e.IdStatusPreporuke).HasColumnName("idStatusPreporuke");

                entity.Property(e => e.Naziv)
                    .HasColumnName("naziv")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Stozer>(entity =>
            {
                entity.HasKey(e => e.IdStozer);

                entity.HasIndex(e => e.IdPredsjednik);

                entity.Property(e => e.IdStozer).HasColumnName("idStozer");

                entity.Property(e => e.IdPredsjednik).HasColumnName("idPredsjednik");

                entity.Property(e => e.Naziv)
                    .HasColumnName("naziv")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Problematika)
                    .HasColumnName("problematika")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Weburl)
                    .HasColumnName("weburl")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdPredsjednikNavigation)
                    .WithMany(p => p.Stozer)
                    .HasForeignKey(d => d.IdPredsjednik)
                    .HasConstraintName("FK__Stozer__idPredsj__4999D985");
            });

            modelBuilder.Entity<TerminObjave>(entity =>
            {
                entity.HasKey(e => e.IdTerminaObjave)
                    .HasName("PK__TerminOb__B474AEC25014B45D");

                entity.Property(e => e.IdTerminaObjave).HasColumnName("idTerminaObjave");

                entity.Property(e => e.Termin)
                    .IsRequired()
                    .HasColumnName("termin")
                    .HasMaxLength(64);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
