﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class PutovanjePodrucje
    {
        public int IdPodrucje { get; set; }
        public int IdPutovanje { get; set; }

        public virtual Podrucje IdPodrucjeNavigation { get; set; }
        public virtual Putovanje IdPutovanjeNavigation { get; set; }
    }
}
