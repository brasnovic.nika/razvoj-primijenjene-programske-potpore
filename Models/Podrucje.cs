﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class Podrucje
    {
        public Podrucje()
        {
            Organizacija = new HashSet<Organizacija>();
            PutovanjePodrucje = new HashSet<PutovanjePodrucje>();
        }

        public int IdPodrucje { get; set; }
        public string Drzava { get; set; }
        public int? IdKategorija { get; set; }
        public long Stanovnistvo { get; set; }
        public string Naziv { get; set; }

        public virtual Kategorija IdKategorijaNavigation { get; set; }
        public virtual ICollection<Organizacija> Organizacija { get; set; }
        public virtual ICollection<PutovanjePodrucje> PutovanjePodrucje { get; set; }
    }
}
