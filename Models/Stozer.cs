﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class Stozer
    {
        public Stozer()
        {
            Preporuka = new HashSet<Preporuka>();
        }

        public int IdStozer { get; set; }
        public int IdPredsjednik { get; set; }
        public string Naziv { get; set; }
        public string Problematika { get; set; }
        public string Weburl { get; set; }

        public virtual Osoba IdPredsjednikNavigation { get; set; }
        public virtual ICollection<Preporuka> Preporuka { get; set; }
    }
}
