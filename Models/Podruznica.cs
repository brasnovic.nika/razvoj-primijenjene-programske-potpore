﻿using System;
using System.Collections.Generic;

namespace RPPP01.Models
{
    public partial class Podruznica
    {
        public int IdPodruznica { get; set; }
        public int IdOrganizacija { get; set; }
        public string Adresa { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }

        public virtual Organizacija IdOrganizacijaNavigation { get; set; }
    }
}
