﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RPPP01.Models;

namespace RPPP01.Controllers
{
    public class StozerController : Controller
    {
        private readonly RPPP01Context _context;

        public StozerController(RPPP01Context context)
        {
            _context = context;
        }

        // GET: Stozer
        public async Task<IActionResult> Index()
        {
            var rPPP01Context = _context.Stozer.Include(s => s.IdPredsjednikNavigation);
            return View(await rPPP01Context.ToListAsync());
        }

        // GET: Stozer/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var stozer = await _context.Stozer
                .Include(s => s.IdPredsjednikNavigation)
                .FirstOrDefaultAsync(m => m.IdStozer == id);
            if (stozer == null)
            {
                return NotFound();
            }

            return View(stozer);
        }

        // GET: Stozer/Create
        public IActionResult Create()
        {
            ViewData["IdPredsjednik"] = new SelectList(_context.Osoba, "IdOsoba", "Ime");
            return View();
        }

        // POST: Stozer/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdStozer,IdPredsjednik,Naziv,Problematika,Weburl")] Stozer stozer)
        {
            if (ModelState.IsValid)
            {
                _context.Add(stozer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdPredsjednik"] = new SelectList(_context.Osoba, "IdOsoba", "Ime", stozer.IdPredsjednik);
            return View(stozer);
        }

        // GET: Stozer/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var stozer = await _context.Stozer.FindAsync(id);
            if (stozer == null)
            {
                return NotFound();
            }
            ViewData["IdPredsjednik"] = new SelectList(_context.Osoba, "IdOsoba", "Ime", stozer.IdPredsjednik);
            return View(stozer);
        }

        // POST: Stozer/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdStozer,IdPredsjednik,Naziv,Problematika,Weburl")] Stozer stozer)
        {
            if (id != stozer.IdStozer)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(stozer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StozerExists(stozer.IdStozer))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdPredsjednik"] = new SelectList(_context.Osoba, "IdOsoba", "Ime", stozer.IdPredsjednik);
            return View(stozer);
        }

        // GET: Stozer/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var stozer = await _context.Stozer
                .Include(s => s.IdPredsjednikNavigation)
                .FirstOrDefaultAsync(m => m.IdStozer == id);
            if (stozer == null)
            {
                return NotFound();
            }

            return View(stozer);
        }

        // POST: Stozer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var stozer = await _context.Stozer.FindAsync(id);
            _context.Stozer.Remove(stozer);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StozerExists(int id)
        {
            return _context.Stozer.Any(e => e.IdStozer == id);
        }
    }
}
