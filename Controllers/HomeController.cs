using Microsoft.AspNetCore.Mvc;

namespace RPPP01.Controllers
{
    public class HomeController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

    }
}