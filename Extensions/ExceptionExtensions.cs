using System;
using System.Text;

namespace RPPP01.Extensions
{
    public static class ExceptionExtensions
    {
        public static string CompleteExceptionMessage(this Exception e)
        {
            StringBuilder sb=new StringBuilder();
            
            while (e!=null)
            {
                sb.AppendLine(e.Message);
                e = e.InnerException;
            }

            return sb.ToString();
        }
    }
}